// Empty JS for your own code to be here
/* DECISION TREE DEFINITION */
tree = {
	0: {
		type: "question",
		text: "Mám potrebné znalosti o nanomateriáloch?",
		options: [
			{
				type: "option",
				text: "Áno",
				node: 1
			},
			{ 
				type: "option",
				text: "Nie",
				node: 2
			}
		]
    },

    1: {
    	type: "question",
		text: "Je nanomateriál, ktorý ma zaujíma úplne nový?",
		options: [
			{
				type: "option",
				text: "Áno",
				node: 3
			},
			{ 
				type: "option",
				text: "Nie",
				node: 4
			}
		]
    },

    2: {
    	type: "methodology",
    	text: ["The NanoDatabase"],
    	href: ["http://nanodb.dk/"],
    	description: "The NanoDatabase je unikátny nástroj, ktorý zhromažďuje informácie o nanomateriáloch. V súčasnosti eviduje takmer 4000 záznamov. Pre každý nanomateriál poskytuje informácie o expozícii pre pracovníkov, spotrebiteľov, životné prostredie a aj hodnotenie nebezpečenstva pre ľudí aj životné prostredie."
    },

    3: {
    	type: "methodology",
    	text: ["LICARA nanoSCAN", "DF4nanoGrouping"],
    	href: ["https://www.empa.ch/web/s506/licara", "https://linkinghub.elsevier.com/retrieve/pii/S0273230015000549"],
    	description: "DF4nanoGrouping je metóda, ktorá zoskupuje nanomateriály podľa ich špecifického spôsobu pôsobenia, ktoré vedie k toxickému účinku. LICARA nanoSCAN je nástroj, ktorý posudzuje prínosy aj riziká nanotechnológii. Založený na hodnotení rizík a a hodnotení životného cyklu bez použitia kvantitatívnych údajov. "
    },

    4: {
    	type: "question",
		text: "Poznám jeho fyzikálno-chemické vlastnosti?",
		options: [
			{
				type: "option",
				text: "Áno",
				node: 5
			},
			{ 
				type: "option",
				text: "Nie",
				node: 6
			}
		]
    },

    5: {
    	type: "question",
		text: "Poznám životný cyklus?",
		options: [
			{
				type: "option",
				text: "Áno",
				node: 8
			},
			{ 
				type: "option",
				text: "Nie",
				node: 9
			}
		]
    },

    6: {
    	type: "question",
		text: "Doporučuje sa využiť: REACHnano ToolKit. Zistil/a som potrebné informácie?",
		options: [
			{
				type: "option",
				text: "Áno",
				node: 5
			},
			{ 
				type: "option",
				text: "Nie",
				node: 7
			}
		]
    },

    7: {
    	type: "methodology",
    	text: ["ECHA/RIVM/JRC read-across and grouping framework for nanomaterials.", "Nanomaterial categorization for assessing risk potential"],
    	href: ["https://echa.europa.eu/documents/10162/13630/eco_toxicological_for_bridging_grouping_nanoforms_en.pdf", "https://pubs.acs.org/doi/10.1021/acsnano.5b00941" ],
    	description: "ECHA/RIV/JRC je určené pre identifikáciu nanoforiem a následné posúdenie, či dostupné údaje postačujú pre identifikáciu nebezpečia alebo je nutné navrhnúť novú testovaciu stratégiuNanomaterial categorization for assessing risk potential je založené na predbežnom zoskupení nanomateriálov k identifikácií príslušných nebezpečenstiev. Na základe identifikovaných údajov môžu potom byť zamerané na podrobnejšie testovanie, analýzu a overovanie. "
    },

    8: {
    	type: "question",
		text: "Používam materiál komerčne v nejakom produkte?",
		options: [
			{
				type: "option",
				text: "Áno",
				node: 10
			},
			{ 
				type: "option",
				text: "Nie",
				node: 11
			}
		]
    },

    9: {
    	type: "methodology",
    	text: ["Nano-Life Cycle Risk Analysis (Nano-LCRA)"],
    	href: [""],
    	description: "Nano-LCRA je založená na adaptívnom riadení a zahŕňa prehodnotenie rámca a predchádzajúcich rozhodnutí na základe nových informácií. Kombinuje životný cyklus materiálu a zároveň nástroje analýzy rizika aby charakterizovala potenciál expozície a riziká nanomateriálov v konkrétnych aplikáciách. "
    },

    10: {
    	type: "question",
		text: "Doporučuje sa využiť metódu Alternatives assessments for nanomaterials. Existuje prijateľnejšia alternatíva?",
		options: [
			{
				type: "option",
				text: "Áno",
				node: 12
			},
			{ 
				type: "option",
				text: "Nie",
				node: 13
			}
		]
    },

    11: {
    	type: "question",
		text: "Očakávam, že bude exponované životné prostredie?",
		options: [
			{
				type: "option",
				text: "Áno",
				node: 14
			},
			{ 
				type: "option",
				text: "Nie",
				node: 15
			}
		]
    },

    12: {
    	type: "methodology",
    	text: ["Použiť iný materiál"],
    	href: [],
    	description: "Ak z metódy Alternatives assessments for nanomaterials vyšiel vhodnejší materiál, odporúča sa použiť ten. "
    },

    13: {
    	type: "methodology",
    	text: ["MARINA FrameWork", "CENARIOS"],
    	href: ["https://www.mdpi.com/1660-4601/12/12/14961", "http://innovationsgesellschaft.ch/wp-content/uploads/2015/01/CENARIOS_Factsheet_englisch_2015.pdf"],
    	description: "MARINA je flexibilný a efektívny prístup k zberu údajov a vyhodnocovaní rizika. Zbiera len údaje potrebné na účely posúdenia rizika. CENARIOS je prvý certifikovaný systém pre riadenie rizík v oblasti nanotechnológii. Vhodný pre priemysel a komerčne zamerané firmy na identifikáciu, analýzu a vyhodnotenie potenciálnych rizík "
    },

    14: {
    	type: "question",
		text: "Očakávam expozíciu aj osôb?",
		options: [
			{
				type: "option",
				text: "Áno",
				node: 16
			},
			{ 
				type: "option",
				text: "Nie",
				node: 17
			}
		]
    },

    15: {
    	type: "question",
		text: "Očakávam, že budú exponovaní pracovníci?",
		options: [
			{
				type: "option",
				text: "Áno",
				node: 20,
			},
			{ 
				type: "option",
				text: "Nie",
				node: 24
			}
		]
    },

    16: {
    	type: "methodology",
    	text: ["SUNDS"],
    	href: ["https://www.sunds.gd/"],
    	description: "Systém na podporu udržateľnosti posudzovania nanoproduktov. Umožňuje podporné rozhodnutia týkajúce sa nanomateriálov spojených s životným cyklom, regulačnými orgánmi a poisťovňami. "
    },

    17: {
	type: "question",
	text: "Bola toxicita nanomateriálu na živ. prostredí už testovaná?",
	options: [
			{
				type: "option",
				text: "Áno",
				node: 18
			},
			{ 
				type: "option",
				text: "Nie",
				node: 19
			}
		]
    },

    18: {
    	type: "methodology",
    	text: ["Comprehensive Environmental Assessment ", "SimpleBox4Nano"],
    	href: [ "https://pubs.acs.org/doi/10.1021/es3023072","https://www.rivm.nl/en/soil-and-water/simplebox4nano"],
    	description: "Comprehensive Environmental Assessment je metóda je dvojrozmerná, poskytuje jednak holistický a systematický spôsob organizácie informácii o zložitých environmentálnych otázkach a zároveň poskytuje transparentné a štruktúrované prostriedky na posúdenie dôsledkov týchto informácii. SimpleBox4nano je systém, ktorý umožňuje vytvárať regulačné multimediálne modely osudov nanomateriálov. Systém predpovedá koncentrácie nanomateriálov vo vzduchu, vode, sedimentoch a pôde."
    },

    19: {
    	type: "methodology",
    	text: ["Test strategy for assessing the risks of nanomaterials in the environment ", "MendNano"],
    	href: ["http://www.enveurope.com/content/27/1/24","https://nanoinfo.org/mendnano/"],
    	description: "Metóda je zameraná na testovanie a posudzovanie rizík nanomateriálov na životné prostredie. Zohľadňuje sa životný cyklus materiálu a jeho potenciál uvoľniť sa do životného prostredia v jednotlivých štádiách cyklu. MendNano popisuje enviroment ako súbor kompartmentov, ktoré zastupujú environmentálne média a biologické entity, ktoré sú vzájomné prepojené. Umožňuje vypočítať koncentrácie nanočastíc v každej komparmente v priebehu času, zohľadňuje procesy prebiehajúce v životnom prostredí ako napr. sedimentácia, vychytávanie koreňmi rastlín či depozícia na listy. "
    },

    20: {
	type: "question",
	text: "Očakávam najmä inhalačnú expozíciu?",
	options: [
			{
				type: "option",
				text: "Áno",
				node: 22
			},
			{ 
				type: "option",
				text: "Nie",
				node: 23
			}
		]
    },

	/*21: {
    	type: "question",
		text: "Očakávam, že budú exponovaní spotrebitelia?",
		options: [
			{
				type: "option",
				text: "Áno",
				node: 24,
			},
			{ 
				type: "option",
				text: "Nie",
				node: 25
			}
		]
    },*/

    22: {
    	type: "methodology",
    	text: ["Risk banding framework"],
    	href: ["http://www.tandfonline.com/doi/full/10.3109/17435390.2015.1132344"],
    	description: "Táto metóda kategorizuje rizika nanočastíc na základe ich fyzikálno-chemických vlastností a expozície. Metóda sa snaží zachytiť základné atribúty, ktoré určujú osud a toxicitu pri vdýchnutí nanočastice a navrhnúť reálnu toxikologickú cestu pre inhalované nanočastice. "
    },

    23: {
    	type: "methodology",
    	text: ["CB NanoTool", "REACHnano ToolKit"],
    	href: ["https://controlbanding.llnl.gov/","http://tools.lifereachnano.eu/"],
    	description: "CB NanoTool umožňuje kvalitatívne hodnotenie rizika súvisiace s expozíciou pracovníkov nanočasticiam v pracovnom prostredí. REACHnano ToolKit je nástroj pre podporu riadenia rizík a na podporu bezpečného používania nanomateriálov počas ich životného cyklu. Obsahuje informácie o 30 najbežnejších nanočasticiach. "
    },

    24: {
    	type: "methodology",
    	text: ["Nano Risk Framework"],
    	href: ["https://nanotech.law.asu.edu/Documents/2011/06/6496_Nano%20Risk%20Framework_534_2973.pdf"],
    	description: "Táto metóda bola navrhnutá tak, aby bola flexibilná. Slúži ako nástroj na organizovanie, dokumentovanie a oznamovanie toho, aké informácie má mať používateľ o materiály a zdôvodnenie rozhodnutí stojacich za manažérskym rozhodovaním."
    },

    /*25: {
    	type: "methodology",
    	text: ["Nanotechnology Risk Governance"],
    	href: [],
    	description: "Blabla"
    },*/
};

method_answer = "Doporučuje sa";
node = 0;
visited_nodes = [];

$( document ).ready(function() {

	function render() {
		$(".option-area").empty();
		$("#previous_node").empty();
		$(".jumbotron").css("display", "none");
		$(".option-area").css("display", "none");
		/* Uzol typu otazka */
		if (tree[node].type == "question") {
			options = tree[node]["options"];
			$("#prompt").text(tree[node].text)

			btnPositive = $("<button/>")
			.attr({
				"type": "button",
				"id": "btn-positive",
				"class": "btn btn-block btn-success",
				"data-node": options[0]["node"]
			})
			.text(options[0]["text"])
			.click(function () { visited_nodes.push(node); node = options[0]["node"]; render() });

			btnNegative = $("<button/>")
				.attr({
					"type": "button",
					"id": "btn-negative",
					"class": "btn btn-block btn-danger",
					"data-node": options[1]["node"]
				})
				.text(options[1]["text"])
				.click(function() { visited_nodes.push(node); node = options[1]["node"]; render() });

			$("#option-positive").append(btnPositive);
			$("#option-negative").append(btnNegative);
			$(".option-area").css("display", "block");
		}

		/* Uzol typu metoda */
		if (tree[node].type == "methodology") {
			$("#prompt").text(method_answer);
			$("#method_name").text(tree[node].text.join(' + '));
			$("#method_description").text(tree[node].description);
			$("#href_area").empty();
			for (i = 0; i < tree[node].href.length; i++) {
				url = (tree[node].href[i]);
				$("#href_area").append($("<p/>").append(
						$("<a/>").attr({
							"class": "btn btn-primary btn-large",
							"href": url
						})
						.text(tree[node].text[i] + " ")
						.append($("<i/>").attr("class", "fas fa-info-circle"))
					))
			}  

			$(".jumbotron").css("display", "block");
			$(".option-area").css("display", "block");
		}

		/* Navigacia */
		btnPrevious = $("<button/>")
				.attr({
					"type": "button",
					"class": "btn btn-primary",
				})
				.text("Späť ")
				.click(function() { node = visited_nodes.pop(); render() })
				.append($("<i/>").attr("class", "fas fa-long-arrow-alt-left"));


		if (node == 0) {
			btnPrevious.prop('disabled', true);
			$("#startAgain").prop('disabled', true);
		} else {
			$("#startAgain").prop('disabled', false);
		}

		$("#previous_node").append(btnPrevious);
		$("#startAgain").click(function() { node = 0; visited_nodes = []; render() });
	}


    console.log( "App ready!" );
    render()
});